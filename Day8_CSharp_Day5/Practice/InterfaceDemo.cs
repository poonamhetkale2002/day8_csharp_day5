﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    interface Figure
    {
        void GetValues();
        void CalculateArea();
        void DisplayDetails();
    }

    class Sqaure:Figure
    {
        double side,area;
        public void CalculateArea()
        {
            area = side * side;
        }
        public void DisplayDetails()
        {
            Console.WriteLine("Area of Square:" + area);
        }
        public void GetValues()
        {
            Console.WriteLine("Enter side of Square:");
            side=Convert.ToDouble(Console.ReadLine());
        }
    }
    class Rectangle : Figure
    {
        double length,height, area;
        public void CalculateArea()
        {
            area = length * height;
        }
        public void DisplayDetails()
        {
            Console.WriteLine("Area of Square:" + area);
        }
        public void GetValues()
        {
            Console.WriteLine("Enter length of rectangle:");
            length= Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter breadth of rectangle:");
            height= Convert.ToDouble(Console.ReadLine());
        }
    }

    class InterfaceDemo
    {
        static void Main()
        {
            Sqaure sqaure = new Sqaure();
            sqaure.GetValues();
            sqaure.CalculateArea();
            sqaure.DisplayDetails();
            Rectangle rectangle = new Rectangle();
            rectangle.GetValues();
            rectangle.CalculateArea();
            rectangle.DisplayDetails();
            Console.ReadLine();
        }
    }
}
