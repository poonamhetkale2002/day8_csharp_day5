﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    struct Record
    {
        public int rn;
        public string name;
        public string batch;
        //public record() { }
        public Record(int rn, string name, string batch)
        {
            this.rn = rn;
            this.batch = batch;
            this.name = name;
        }
        public void Show()
        {
            Console.WriteLine("ROll No:" + rn);
            Console.WriteLine("Name:" + name);
            Console.WriteLine("batch Name:" + batch);
        }
    }
    class StructureDemo
    {
        static void Main()
        {
            Record rec = new Record();
            rec.rn = 6115;
            rec.name = "Poonam";
            rec.batch = "Batch B2";
            rec.Show();
        }
    }
}
