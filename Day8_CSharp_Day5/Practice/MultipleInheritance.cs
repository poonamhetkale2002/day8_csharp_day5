﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    interface Figure1
    {
        void GetValues();
       
        void DisplayDetails();
    }
    interface Area
    {
        void CalculateArea();
    }


    class Sqaure1 : Figure, Area
    {
        double side, area;
        public void CalculateArea()
        {
            area = side * side;
        }
        public void DisplayDetails()
        {
            Console.WriteLine("Area of Square:" + area);
        }
        public void GetValues()
        {
            Console.WriteLine("Enter side of Square:");
            side = Convert.ToDouble(Console.ReadLine());
        }
    }
    class Rectangle1 : Figure,Area
    {
        double length, height, area;
        public void CalculateArea()
        {
            area = length * height;
        }
        public void DisplayDetails()
        {
            Console.WriteLine("Area of Square:" + area);
        }
        public void GetValues()
        {
            Console.WriteLine("Enter length of rectangle:");
            length = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter breadth of rectangle:");
            height = Convert.ToDouble(Console.ReadLine());
        }
    }

    class MultipleInheritance
    {
        static void Main()
        {
            Sqaure1 sqaure = new Sqaure1();
            sqaure.GetValues();
            sqaure.CalculateArea();
            sqaure.DisplayDetails();
            Rectangle1 rectangle = new Rectangle1();
            rectangle.GetValues();
            rectangle.CalculateArea();
            rectangle.DisplayDetails();
            Console.ReadLine();
        }
    }
}
