﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    // enum choice {add,sub,mul,div };  //by default index start with 0
    enum choice { add=1, sub, mul, div };  //now index start with 1

    class EnumDemo
    {
        static void Main()
        {
            Console.WriteLine("Enter value of num1:");
            int num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter value of num2:");
            int num2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter your Choice: 1.add 2.sub 3.mul 4.div==>");
            int ch=Convert.ToInt32(Console.ReadLine());

            switch (ch)
            {
                case (int)choice.add: Console.WriteLine("Addition:" + (num1 + num2)); break;
                case (int)choice.sub: Console.WriteLine("Subtraction:"+ (num1 -num2)); break;
                case (int)choice.mul: Console.WriteLine("Multiplication:"+(num1 *num2)); break;
                case (int)choice.div: Console.WriteLine("Division;" + (num1 / num2)); break;
                default: Console.WriteLine("invalid Choice."); break;
            }
        }
    }
}
