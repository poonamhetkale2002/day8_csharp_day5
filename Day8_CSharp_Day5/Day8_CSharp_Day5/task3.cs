﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day8_CSharp_Day5
{
    /*
        Implement a priority queue in C# to manage emergency room admissions
        in a hospital. Patients with higher urgency should be served before 
        those with lower urgency.
     */
    public class Patient
    {
        public string Name { get; set; }
        public int Urgency { get; set; }

        public Patient(string name, int urgency)
        {
            Name = name;
            Urgency = urgency;
        }
    }

    public class PriorityQueue
    {
        private List<Patient> patients = new List<Patient>();

        public void Enqueue(Patient patient)
        {
            patients.Add(patient);
            int curidx = patients.Count - 1;

            while (curidx > 0)
            {
                int paridx= (curidx - 1) / 2;
                if (patients[paridx].Urgency < patients[curidx].Urgency)
                {
                    Swap(curidx, paridx);
                    curidx = paridx;
                }
                else
                {
                    break;
                }
            }
        }

        public Patient Dequeue()
        {
            if (patients.Count == 0)
                Console.WriteLine("Queue is empty");

            Patient maxPriority = patients[0];
            patients[0] = patients[patients.Count - 1];
            patients.RemoveAt(patients.Count - 1);

            HeapifyDown();
            return maxPriority;
        }

        private void HeapifyDown()
        {
            int currentIndex = 0;

            while (true)
            {
                int left= 2 * currentIndex + 1;
                int right= 2 * currentIndex + 2;
                int maxIndex = currentIndex;

                if (left< patients.Count && patients[left].Urgency > patients[maxIndex].Urgency)
                {
                    maxIndex = left;
                }

                if (right < patients.Count && patients[right].Urgency > patients[maxIndex].Urgency)
                {
                    maxIndex = right;
                }

                if (maxIndex != currentIndex)
                {
                    Swap(currentIndex, maxIndex);
                    currentIndex = maxIndex;
                }
                else
                {
                    break;
                }
            }
        }

        private void Swap(int index1, int index2)
        {
            Patient temp = patients[index1];
            patients[index1] = patients[index2];
            patients[index2] = temp;
        }

        public int Count
        {
            get { return patients.Count; }
        }
    }
    class task3
    {        
        static void Main()
        {
            PriorityQueue x= new PriorityQueue();
            x.Enqueue(new Patient("abc", 2));
            x.Enqueue(new Patient("pqr", 5));
            x.Enqueue(new Patient("lmn", 6));
            x.Enqueue(new Patient("Komal", 1));
            while (x.Count > 0)
            {
                Patient nextPatient = x.Dequeue();
                Console.WriteLine("Next patient: {0} - Urgency: {1}", nextPatient.Name, nextPatient.Urgency);
            }
        }
    }
}

