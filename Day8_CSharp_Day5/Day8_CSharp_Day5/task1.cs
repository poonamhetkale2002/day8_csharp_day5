﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day8_CSharp_Day5
{
    /*
     Task 1: Implementing a Linked List
        1) Write a C# class CustomLinkedList that implements a 
        singly linked list with methods for InsertAtBeginning, 
        InsertAtEnd, InsertAtPosition, DeleteNode, UpdateNode, 
        and DisplayAllNodes. Test the class by performing a 
        series of insertions, updates, and deletions.
     */

    class Node
    {
        public int Data;
        public Node next;
        
    }
    public class CustomLinkedList
    {
        Node start = null, last, ptr, prev, New;
        Node create()
        {
            New = new Node();
            New.Data = 0;
            New.next = null;
            return New;
        }

        public void InsertAtBeginning(int x)
        {
            Node p,q;
            p = start;
            if(p == null)
            {
                p=create();
                p.Data = x;
                p.next = null;
                start = p;
            }
            else
            {
                q=create();
                q.Data = x;
                q.next = p;
                start = q;
            }
            Console.WriteLine(x + " added at begining.");
        }

        public void InsertAtEnd(int x)
        {
            Node p,q;
            p = start;
            if(p == null) 
            { 
                p=create();
                p.Data = x;
                p.next = null;
                start = p;
            }
            else
            {
                while(p.next != null)
                {
                    p = p.next;
                }
                q=create();
                q.Data = x;
                q.next = null;
                p.next=q;
            }
            Console.WriteLine(x + " added at end.");
        }
        public void InsertInBetween(int after,int x)
        {
            Node p, q;
            p=start;
            if(p == null || p.next==null)
            {
                Console.WriteLine("Insert in Between is not possible.");
            }
            else
            {
                while(p.next != null)
                {
                    if(after==p.Data)
                    {
                        q = create();
                        q.Data = x;
                        q.next = p.next;
                        p.next = q;
                    }
                    p=p.next;
                }
                Console.WriteLine(x + " added in between.");
            }

        }

        public int DeleteNode()
        {
            int n;
            Node p;
            p = start;
            if (p == null)
                Console.WriteLine("Linked list is empty .");
            else if(p.next == null)
            {
                n = p.Data;
                start = null;
                return n;
            }
            else
            {
                n = p.Data;
                start = p.next;                
                return n;
            }
            return 0;
        }

        public void DisplayAllNodes()
        {
            Node p;
            p = start;
            while(p != null)
            {
                Console.WriteLine(p.Data);
                p=p.next;
            }
        }   
        
        public void UpdateNode(int item , int update)
        {
            
            Node p;int n;
            p= start;
            while(p!= null)
            {
                if(p.Data==item)
                {
                    n = p.Data;
                    p.Data = update;
                }
                p = p.next;
            }
            Console.WriteLine(update + " updated successfully.");
        }
    }
class task1
    {
        static void Main()
        {
            CustomLinkedList x=new CustomLinkedList();

            int ch;
            do
            {
                Console.WriteLine("Enter your choice: 1.InsertBegining 2.InsertEnd 3.DisplayAll 4.InsertAtPosition 5.DeleteNode 6.UpdateNode 0.Exit===>");
                
                ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        {
                            Console.WriteLine("Enter item to insert at begining:");
                            int item = Convert.ToInt32(Console.ReadLine());
                            x.InsertAtBeginning(item);
                        }
                        break;

                    case 2:
                        {
                            Console.WriteLine("Enter item to insert at end:");
                            int item = Convert.ToInt32(Console.ReadLine());
                            x.InsertAtEnd(item);
                        }
                        break;
                    case 3: x.DisplayAllNodes(); break;
                    case 4:
                        {
                            Console.WriteLine("after  which item(value/data) you want to insert ?:");
                            int after = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter item to insert in between:");
                            int item = Convert.ToInt32(Console.ReadLine());
                            x.InsertInBetween(after,item);
                        }
                        break;
                    case 5:
                        {
                            int z = x.DeleteNode();
                            Console.WriteLine(z+ " removed successfully.");
                        }
                        break;
                    case 6:
                        {
                            Console.WriteLine("All items are :");
                            x.DisplayAllNodes();
                            Console.WriteLine("which item you want to update?:");
                            int item = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("which item you want to place in that position?:");
                            int update= Convert.ToInt32(Console.ReadLine());
                            x.UpdateNode(item,update);
                        }
                        break;
                    case 0: break;
                    default:
                        Console.WriteLine("invalid Choice.....");
                        break;
                }

            } 
            while (ch != 0) ;            
        }
    }
}
